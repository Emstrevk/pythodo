import sqlite3
import time

class database():
    def __init__(self):
        self.db = sqlite3.connect('pythodo.db')

    def log(self, tag, message):
        f = open("./pythodoLog", 'a')
        tag = "[" + tag + "]: "
        timeTag = " (" + time.strftime("%d/%m/%Y") + ")"
        f.write("\n" + tag + message + timeTag)
        f.close()

    def remove_entry(self, id, body):
        self.log("Removed", body)

        with self.db:
            cursor = self.db.cursor()
            cursor.execute("DELETE FROM entries WHERE id = ?", (id,))

    def add_entry(self, body):
        self.log("Added", body)
        with self.db:
            cursor = self.db.cursor()
            cursor.execute("INSERT INTO entries (id, body) values (NULL,?)", (body,))


    # Get all entries as list of dicts
    def get_entries(self):
        dictList = []

        with self.db:
            cursor = self.db.cursor()
            cursor.execute("SELECT id, body FROM entries")
            curList = cursor.fetchall()

        for row in curList:
            item = {}
            item['id'] = row[0]
            item['body'] = row[1]
            dictList.append(item)

        return dictList

    # Get all sub-entries as list of dicts
    def get_sub_entries(self):
        dictList = []

        with self.db:
            cursor = self.db.cursor()
            cursor.execute("SELECT id, parent_id, body FROM children")
            curList = cursor.fetchall()

        for row in curList:
            item = {}
            item['id'] = row[0]
            item['parent_id'] = row[1]
            item['body'] = row[2]
            dictList.append(item)

        return dictList
