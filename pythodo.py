import curses
from curses import wrapper
from terminal import terminal
from database import database
import time


def main(stdscr):
    db = database()
    ui = terminal(stdscr, db)

    ui.load_entries()

    while True:
        # Do whatever
        ui.update()
        time.sleep(0.06)



## BEGIN

wrapper(main)
