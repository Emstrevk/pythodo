import curses
import pygame
import os
import time

class terminal():
    def __init__(self, screen, datab):
        self.screen = screen
        self.clear()
        curses.use_default_colors()
        self.db = datab

        self.animate_cursor = False

        #Positional variables
        self.cursor_y = 0
        self.entries_start_y = 3
        self.entry_indent = 2 #int( curses.COLS / 3 )
        self.current_line = self.entries_start_y
        self.cursor_y = self.current_line


        #Hide cursor
        curses.curs_set(0)

    def clear(self):
        self.screen.clear()
        self.draw_shortcuts()

    def load_entries(self):
        self.clear()
        self.entries = self.db.get_entries()
        self.current_line = self.entries_start_y

        index = 1

        for e in self.entries:
            to_type = e['body']#"(" + str(index) + ") " + e['body']
            self.screen.addstr(self.current_line, self.entry_indent, to_type)
            #self.slow_write(self.current_line, self.entry_indent, to_type)
            self.current_line += 1
            index += 1

        self.screen.refresh()
        self.update_cursor(True, True)

    def update(self):
        self.update_cursor(False, True)
        c = self.screen.getch()
        #key = curses.keyname(c)
        #self.message(key)


        if (c == curses.KEY_DOWN):
            self.update_cursor(False)
        elif (c == curses.KEY_UP):
            self.update_cursor(True)
        elif (c == ord('a')):
            self.add_entry()
        elif (c == ord('r')):
            self.remove_entry()
        elif (c == ord('s')):
            self.add_sub_entry()

    def slow_write(self, y, x, string):
        sleepTime = 0.02
        debug = False

        for c in string:

            self.screen.addstr(y,x,c)
            x += 1
            self.screen.refresh()
            time.sleep(sleepTime)

        return

    def update_cursor(self, upward, init = False):
        old_y = self.cursor_y
        if (self.animate_cursor):
            self.animate_cursor = False
            cursor = "|❙"
        else:
            self.animate_cursor = True
            cursor = ">"

        max_y = self.entries_start_y + len(self.entries) - 1

        if ( not init):
            if (upward):
                self.cursor_y -= 1
            else:
                self.cursor_y += 1

        #self.message("Key pressed: " +  str(ord(key)))

        if (self.cursor_y < self.entries_start_y):
            self.cursor_y = self.entries_start_y
        elif(self.cursor_y > max_y):
            self.cursor_y = max_y


        self.screen.addstr(old_y, 0, "  ")
        self.screen.addstr(self.cursor_y, 0, cursor)
        self.screen.refresh()

    def remove_entry(self):
        cursor_index = self.cursor_y - self.entries_start_y
        if (len(self.entries) > 0):

            entry = self.entries[cursor_index]
            self.db.remove_entry(entry['id'], entry['body'])
            self.load_entries()

        self.message(entry['body'])

    def add_sub_entry(self):
        lol = "lol"

    def add_entry(self):

        curses.curs_set(1)

        read_y = self.entries_start_y + len(self.entries)
        read_x = self.entry_indent
        curses.echo()
        input = self.screen.getstr(read_y, read_x, 100).decode(encoding="utf-8")
        curses.noecho()
        self.db.add_entry(input)
        self.screen.refresh()
        curses.curs_set(0)
        eraser = " " * len(input)
        self.screen.addstr(read_y, read_x, eraser)
        self.load_entries()
        self.message(input)


    def message(self, msg):

        self.slow_write(0, 10, msg)
        eraser = " " * len(msg)
        self.slow_write(0, 10, eraser)

    def draw_shortcuts(self):

        self.screen.addstr(0, 0, "~pythodo")

        divider = "_" * ( curses.COLS )

        add_text = "a: add entry"
        remove_text = "r: remove entry"
        sub_text = "s: add sub-entry"

        y_line = curses.LINES - 1
        x_first = 0
        x_second = len(add_text) + 10
        x_third = x_second + len(remove_text) + 10

        #self.screen.addstr(y_line - 1, x_first, divider)
        self.screen.addstr(y_line, x_first, add_text)
        self.screen.addstr(y_line, x_second, remove_text)

        self.screen.addstr(y_line, x_third, sub_text)

        self.screen.refresh()
